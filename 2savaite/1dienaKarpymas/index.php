<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Spalvyno karpymas</title>
	<link href="https://fonts.googleapis.com/css?family=Alegreya+SC:700|Trocchi" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<section class="first">
		<div class="wrapper">
			<header>
				<nav>
					<img src="img/spalvynas-logo.png" alt="Spalvyno logo">
					<ul>
						<li><a href="#">Apie mus</a></li>
						<li><a href="#">Paslaugos</a></li>
						<li><a href="#">Kodėl mes?</a></li>
						<li><a href="#">Kaip mes dirbame?</a></li>
						<li><a href="#">Kontaktai</a></li>
					</ul>
				</nav>
			</header>
			<div>
				<div>
					<img src="img/ragelis.png" alt="Telefonas skambinti Marytei">
					<a href="tel: +370 99999999">
						<p>+370 99999999</p>
					</a>
				</div>
			</div>
			<h1>Dažų gamyba ir prekyba</h1>
			<h2>Automobiliams, laivams, metalinems konstrukcijoms ir t.t.</h2>
			<img src="img/arrow.png">
		</div>
	</section>
	<section class="second">
		<div class="wrapper">
			<h2>
				<img src="img/spalvynas-logo-zodis.png" alt="">TAI - 
			</h2>
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<h3>Patirtis</h3>
						<p>Dirbame daugiau <br>kaip 22 <br>metus</p>
					</div>
					<div class="col-md-4">
						<h3>tęstinumas</h3>
						<p>Esame padarę dažus perdažimui daugiau nei 20000 daužtų mašinų</p>
					</div>
					<div class="col-md-4">
						<h3>patikimumas</h3>
						<p>50 pastovių klientų su kuriais dirbame daugiau kaip 20 metų</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3>darbštumas</h3>
						<p>Vien per pastaruosius metus turėjome 500 klientų</p>
					</div>
					<div class="col-md-4">
						<h3>efektyvumas</h3>
						<p>Greito užsakymo galimybė. Įprastų užsakymų vykdymas<br> iki 1 dienos</p>
					</div>
					<div class="col-md-4">
						<h3>įvairovė</h3>
						<p>Sunaudojame 100kg <br>dažų naujų spalvų gamybai</p>
					</div>
				</div>
			</div>

		</div>
	</section>
</body>
</html>