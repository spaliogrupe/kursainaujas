//1a

// var paimtasDivas = document.getElementById('moku');
// paimtasDivas.innerHTML = "Man pavyko";

//1b
// paimtasDivas.style.backgroundColor = "green";
//1c
// paimtasDivas.style.height = "300px";
// paimtasDivas.style.width = "300px";
// console.log(paimtasDivas);
//2a
//
// var sukurtasDiv = document.createElement('div');
// sukurtasDiv.id = "moku";
// var pavyko = document.body.appendChild(sukurtasDiv);
// //2b
// sukurtasDiv.innerHTML = "Valio pavyko";
// //2c
// sukurtasDiv.style.height = "300px";
// sukurtasDiv.style.width = "300px";
// sukurtasDiv.style.textAlign = "center";
// sukurtasDiv.style.backgroundColor = "green";
// sukurtasDiv.style.margin = "0 auto";
//
// //2d
// var mygtukas = document.createElement('button');
// mygtukas.id = "spaudaliukas";
// mygtukas.innerHTML = "Spausk";
// var pridetiMygtuka = document.body.appendChild(mygtukas);
// //2e
// document.getElementById('spaudaliukas').addEventListener('click', function(){
//     var abc = document.getElementById('moku');
//
//     abc.style.display = 'none';
//
//     document.getElementById('spaudaliukas').style.display = 'none';
// });

//3a
// var karambaDiv = document.createElement('button');
// karambaDiv.id = "karamba";
// karambaDiv.innerHTML = "Spausk!!!!";
// document.body.appendChild(karambaDiv);
//
// document.getElementById('karamba').addEventListener('click', function () {
//     //Sukursiu elementa
//     var framas = document.createElement('iframe');
//     //Sudeti stilius
//     framas.style.width = '560px';
//     framas.style.height = '315px';
//     framas.src = "https://www.youtube.com/embed/mHLLclRQ10o";
//     framas.frameBorder = 0;
//     framas.style.display = "block";
//     framas.style.margin = "0 auto";
//     framas.setAttribute('allowfullscreen', '');
//     //Prideti ta elementa i html
//     document.body.appendChild(framas);
// });
// <iframe width="560" height="315" src="https://www.youtube.com/embed/mHLLclRQ10o" frameborder="0" allowfullscreen></iframe>

// document.getElementById('spaudykle').addEventListener('click', function(){
//    // Paimti reiksmes
//     var kainaReiksme = document.getElementById('kaina').value;
//     var kiekioReiksme = document.getElementById('kiekis').value;
//     var pavadinimoReiksme = document.getElementById('pavadinimas').value;
//    // Pasinaudojus reiksmen suskaiciuoti
//     var suma = kainaReiksme * kiekioReiksme;
//    // Atvaizduoti atgal atsakyma
//    // a) Sukurti elementa.
//     var naujasElementas = document.createElement('p');
//     naujasElementas.id = "vasia";
//     console.log(naujasElementas);
//    // b) prilyginti elemento reiksme suskaiciuotai reiksmei
//     naujasElementas.innerHTML = "Vaisiu pavadinimas: " + pavadinimoReiksme + " " + suma + " " + "EUR";
//     document.body.appendChild(naujasElementas);
//    // Nunulinti/isvalyti inputus
// });

// var pc = {};
// pc.gamintojas = "lenovo";
// pc.os = ["windows", "linux"];
// pc.likutis = 3;
// pc.parametrai = {
//     "procesorius": "i7",
//     "ram" : 8,
//     "atmintis":240,
//     "lieciamas ekranas": true
// };
//
// console.log(pc);
// console.log(pc.gamintojas);
// console.log(pc.os[1]);
// console.log(pc.parametrai.atmintis);
// console.log(pc.parametrai["lieciamas ekranas"]);

// Objektu uzduotis Nr.1
//
// var Linas = {};
// Linas.greitis = 15;
// Linas.vardas = "Linas";
// Linas.pavarde = "Auksine";
// Linas.ugis = "2.00m";
// Linas.amzius = 31;
//
// Linas.showGreitis = function (){
//     return Linas.greitis;
// };
// // console.log(Linas.showGreitis());
//
// var Petras = {};
// Petras.greitis = 2;
// Petras.vardas = "Petras";
// Petras.pavarde = "Raiduotis";
// Petras.ugis = "2.00m";
// Petras.amzius = 31;
//
// Petras.showGreitis = function (){
//     return Petras.greitis;
// };
//
// var Martynas = {};
// Martynas.greitis = 10;
// Martynas.vardas = "Martynas";
// Martynas.pavarde = "Burokauskas";
// Martynas.ugis = "2.00m";
// Martynas.amzius = 31;
//
// Martynas.showGreitis = function (){
//     return Martynas.greitis;
// };
//
// // Susideti greiti i array;
//
// var naujasArray = [Linas, Petras, Martynas];
// var pagalVarda = [];
// var pagalPavarde = [];
// var pagalGreiti = [];
// for (i=0; i<naujasArray.length; i++){
//     //paimti is array greiti su vardu ir patikrinti, kuris greiciausias.
//
//     // pagal greiti sudeti i kita array
//     // console.log(naujasArray[i].vardas);
//     // console.log(naujasArray[i].showGreitis());
//     // pagalGreiti.push(naujasArray[i].greitis);
//     // Pagal varda
//     pagalVarda.push(naujasArray[i].vardas);
//     pagalPavarde.push(naujasArray[i].pavarde);
// }
// // Array atspausdinti per for ir tikrinti, kuri reiksme didesne.
// console.log(pagalVarda.sort());
// console.log(pagalPavarde.sort());
// // console.log(pagalGreiti.sort());
//
// //sortinimo algoritmai
// // function compare(a,b) {
// //     if (a.last_nom < b.last_nom)
// //         return -1;
// //     if (a.last_nom > b.last_nom)
// //         return 1;
// //     return 0;
// // }
// // objs.sort(compare);
//
// //sortinimo algoritmas objektu array pagal savybe
// function dynamicSort(property) {
//     var sortOrder = 1;
//     if(property[0] === "-") {
//         sortOrder = -1;
//         property = property.substr(1);
//     }
//     return function (a,b) {
//         var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
//         return result * sortOrder;
//     }
// }
// // naujasArray.sort(dynamicSort("greitis"));
// // var maxSpeed = {
// //     Lino: 15,
// //     Petro: 2,
// //     Martyno: 10
// // };
// // console.log(naujasArray);
// // var sortable = [];
// // for (var zmogus in maxSpeed) {
// //     sortable.push([zmogus, maxSpeed[zmogus]]);
// // }
// // sortable.sort(function(a, b) {
// //     return b[1]- a[1];
// // });
// // console.log(sortable);
//
// var masinyte = function(pavadinimas, greitis, kelias) {
//     this.name = pavadinimas;
//     this.speed = greitis;
//     this.distance = kelias;
//     this.getName = function(){
//         return this.name;
//     };
//     this.getSpeed = function(){
//         return this.speed;
//     };
//     this.getDistance = function(){
//         return this.distance;
//     }
// };
// var tackes = [];
// document.getElementById('prideti').addEventListener('click', function(){
//     var kelioReiksme = document.getElementById('kelias').value;
//     var greicioReiksme = document.getElementById('greitis').value;
//     var masinosPavadinimas = document.getElementById('tackesvards').value;
//     tackes.push(new masinyte(masinosPavadinimas, greicioReiksme, kelioReiksme));
//     var karambaDiv = document.createElement('p');
//     karambaDiv.innerHTML = masinosPavadinimas + " " + greicioReiksme;
//     document.getElementById('bortas').appendChild(karambaDiv);
//
//     if(tackes.length > 2){
//         // naujasArray.sort(dynamicSort("greitis"));
//         tackes.sort(dynamicSort('speed'));
//         console.log(tackes);
//         var laimetojas = document.createElement('p');
//         laimetojas.innerHTML = "greiciausias automobilis " + tackes[0].name;
//         document.getElementById('bortas').appendChild(laimetojas);
//     }
//     console.log(tackes);
// });

// Ar veikia jQuery

$(function() {
   // $('.linksmuolis ul> li').css('color', 'violet');
   // $('#bam').css('color', 'violet');

    // var coffee = [
    //     { name: "Edita", quantity: "belenkoki kieki"},
    //     { name: "As", quantity: "0-1"},
    //     { name: "Irena", quantity: "2"},
    //     { name: "Gintaras", quantity: "3"},
    //     { name: "Justas", quantity: "3"}
    // ];
    // $.each(coffee, function (index, element) {
    //     $('ul').append('<li>' + element.name + ' isgeria ' + element.quantity + ' puodelius kavos per dieną' + '</li>');
// });

    function euraiIsvarus(eurai) {
        return eurai*0.9120;
    }
    function svaraiIeurus(svarai){
        return svarai*0.8680;
    }

    $('#eurai').keyup(function () {
        // paimti imputo reiksme
        var suJS = document.getElementById('eurai').value;
        var atsakymasIsvarus = euraiIsvarus(suJS);
        document.getElementById('svarai').value = atsakymasIsvarus;
        // var EuraiInput =$('#eurai').val();
        // var atsakymasIsvarus = euraiIsvarus(EuraiInput);
        // var svaraiInput =$('#svarai').val(atsakymasIsvarus);
    });
    $('#svarai').keyup(function () {
        // paimti imputo reiksme
        var svaraiInput =$('#svarai').val();
        var atsakymasIeurus = svaraiIeurus(svaraiInput);
        var svaraiEurus =$('#eurai').val(atsakymasIeurus);
    });
});










