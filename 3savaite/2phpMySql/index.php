<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ne labai fantastish!!</title>
</head>
<body>
<form action="fantastish.php" method="get">
    <div>
        <label for="">Vardas</label>
        <input type="text" name="vardas">
    </div>
    <div>
        <label for="">Pavarde</label>
        <input type="text" name="pavarde">
    </div>
    <div>
        <label for="">Amzius</label>
        <input type="number" name="amzius">
    </div>
    <div>
        <select name="lytis">
            <option value="1">Vyras</option>
            <option value="0">Moteris</option>
        </select>
    </div>
    <button>Uzsiregistruoti</button>
</form>
</body>
</html>